from distutils.log import debug
from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from  werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import create_engine
from datetime import datetime, timedelta
from flask_sqlalchemy import SQLAlchemy
import jwt
from functools import wraps

app = Flask(__name__)

app.config['SECRET_KEY'] = 'phamthanhloc'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:docker@localhost:5432/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

def get_db_connection():
    db_connect = create_engine('postgresql://postgres:docker@localhost:5432/postgres')
    conn = db_connect.connect()
    return conn

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message' : 'Token required'}), 401
  
        try:
            # decoding the payload to fetch the stored details
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
            conn = get_db_connection()
            current_user = conn.execute('SELECT * FROM TESTDATA.CUSTOMERS WHERE id = %s', data['id']).first()
        except:
            return jsonify({
                'message' : 'Token is invalid !!'
            }), 401
        # returns the current logged in users contex to the routes
        return  f(current_user, *args, **kwargs)
  
    return decorated

@app.route('/update-customer', methods=['PUT'])
@token_required
def update_customer(self):
    data = request.form
    name, email, age = data.get('name'), data.get('email'),data.get('age')
    conn = get_db_connection()
    result = {'email': email}
    if name:
        name = data.get('name')
        conn.execute('UPDATE TESTDATA.CUSTOMERS SET name = %s WHERE email = %s',(name,email))
        result['name'] = name
    if age:
        age = data.get('age')
        conn.execute('UPDATE TESTDATA.CUSTOMERS SET age = %s WHERE email = %s',(age,email))  
        result['age'] = age  
    conn.close()
    return jsonify(result)

@app.route('/delete-customer', methods=['DELETE'])
@token_required
def delete_customer(self):
    email = request.args.get('email')
    conn = get_db_connection()
    conn.execute('DELETE FROM TESTDATA.CUSTOMERS WHERE email = %s', email)
    conn.close()
    return 'success'
    
@app.route('/get-customers', methods=['GET'])
@token_required
def get_all_customers(self):
    conn = get_db_connection()
    query = conn.execute("select * from testdata.customers").fetchall()
    result = []
    for customer in query:
        result.append({
            'name': customer.name,
            'age':customer.age,
            'email':customer.email
        })    
    
    return jsonify({'customer':result})

@app.route('/login', methods =['POST'])
def login():
    auth = request.form
    if not auth or not auth.get('email') or not auth.get('password'):
        # returns 401 if any email or / and password is missing
        return make_response(
            'Could not verify',
            401,
            {'WWW-Authenticate' : 'Basic realm ="Login required"'}
        )
        
    conn = get_db_connection()
    customer = conn.execute('SELECT * from TESTDATA.CUSTOMERS WHERE email = %s', auth.get('email')).first()
    conn.close()
        
    if not customer:
        # returns 401 if user does not exist
        return make_response(
            'Could not verify',
            401,
            {'WWW-Authenticate' : 'Basic realm ="User does not exist"'}
        )
    
    if check_password_hash(customer.password, auth.get('password')):
        # generates the JWT Token
        token = jwt.encode({
            'id': customer.id,
            'exp' : datetime.utcnow() + timedelta(minutes = 30)
        }, app.config['SECRET_KEY'])
  
        return make_response(jsonify({'token' : token}), 201)
    
    return make_response(
        'Could not verify',
        403,
        {'WWW-Authenticate' : 'Basic realm ="Wrong Password"'}
    )
    
@app.route('/signup', methods =['POST'])
def signup():
    # creates a dictionary of the form data
    data = request.form
  
    # gets name, email and password
    name, email, age = data.get('name'), data.get('email'),data.get('age')
    password = data.get('password')
  
    # checking for existing user
    conn = get_db_connection()
    customer = conn.execute('SELECT * FROM TESTDATA.CUSTOMERS WHERE email = %s', email).first()

    if not customer:
        password = generate_password_hash(password)
        # insert user
        conn.execute('INSERT INTO TESTDATA.CUSTOMERS(name, age, email, password) VALUES(%s,%s,%s,%s)', (name, age, email, password))
        conn.close()
        return make_response('Successfully registered.', 201)
    else:
        # returns 202 if user already exists
        return make_response('User already exists. Please Log in.', 202)

if __name__ == '__main__':
     app.run()