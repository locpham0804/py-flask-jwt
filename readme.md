## HOST POSTGRES DATABASE
Install Docker
Start postgresql database:
cmd:
```
docker run --name postgres-db -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres
```
cmd: ipconfig => ipv4(WSL)
connect with postgresql by azure data studio
SQL > 
```
CREATE SCHEMA testdata
CREATE TABLE testdata.customers(
id SERIAL PRIMARY KEY,
name VARCHAR(250),
age INT,
email VARCHAR(250),
password VARCHAR(100)
)
```
## RUN PYTHON APP
cmd:
Install dependencies: 
```
pip install -r requirements.txt
```
Run project: 
```
python app.py
```
using postman to test API:
Sign Up Customer (CREATE):
![Untitled](/uploads/0ad875dd5d42dda501cd40ac993e3123/Untitled.png)
Login (AUTH):
![Untitled1](/uploads/ad6afbf72ad2396d2381e21f690b20e2/Untitled1.png)
Get Customer (READ):
![Untitled2](/uploads/1c92fe386f8313f7961853b751f242e2/Untitled2.png)
Update Customer (UPDATE):
![Untitled3](/uploads/8a5a9b3a109297bbe7d136eba46a0759/Untitled3.png)
Delete Customer (DELETE):
![Untitled4](/uploads/bcece29dc32d9e6809bc87b1de49e97d/Untitled4.png)
